# gitlab-runner

A gitlab runner module for installing, configuring, registering, and starting the service on a local system.  This module will download a binary from s3 and set the permissions.  It will then configure the config file in `~/.gitlab-runner/config.toml`.  It will then register the runner to the desired gitlab instance and finally, it will start the service.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Example

```hcl
module "gitlab_runner" {
  source             = "../.."
  name               = "test-gitlab-runner"
  runner_tags        = ["test-tag"]
  registration_token = "12345566"
  gitlab_token       = "asbashgdslkag"
  context            = module.this.context
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_access_level"></a> [access\_level](#input\_access\_level) | Runner build directory | `string` | `"not_protected"` | no |
| <a name="input_additional_tag_map"></a> [additional\_tag\_map](#input\_additional\_tag\_map) | Additional key-value pairs to add to each map in `tags_as_list_of_maps`. Not added to `tags` or `id`.<br>This is for some rare cases where resources want additional configuration of tags<br>and therefore take a list of maps with tag key, value, and additional configuration. | `map(string)` | `{}` | no |
| <a name="input_allow_untagged"></a> [allow\_untagged](#input\_allow\_untagged) | Runner build directory | `bool` | `false` | no |
| <a name="input_attributes"></a> [attributes](#input\_attributes) | ID element. Additional attributes (e.g. `workers` or `cluster`) to add to `id`,<br>in the order they appear in the list. New attributes are appended to the<br>end of the list. The elements of the list are joined by the `delimiter`<br>and treated as a single ID element. | `list(string)` | `[]` | no |
| <a name="input_aws_access_key"></a> [aws\_access\_key](#input\_aws\_access\_key) | The access key specified for your S3 instance. | `string` | `""` | no |
| <a name="input_aws_secret_key"></a> [aws\_secret\_key](#input\_aws\_secret\_key) | The secret key specified for your S3 instance. | `string` | `""` | no |
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | Name of the storage bucket where cache is stored. | `string` | `""` | no |
| <a name="input_bucket_region"></a> [bucket\_region](#input\_bucket\_region) | Name of S3 region. | `string` | `"us-east-2"` | no |
| <a name="input_build_dir"></a> [build\_dir](#input\_build\_dir) | Absolute path to a directory where builds are stored in the context of the selected executor. | `string` | `"/tmp"` | no |
| <a name="input_cache_path"></a> [cache\_path](#input\_cache\_path) | Name of the path to prepend to the cache URL | `string` | `""` | no |
| <a name="input_cache_share"></a> [cache\_share](#input\_cache\_share) | Enables cache sharing between runners | `bool` | `true` | no |
| <a name="input_check_interval"></a> [check\_interval](#input\_check\_interval) | Defines the interval length, in seconds, between the runner checking for new jobs. The default value is 3. If set to 0 or lower, the default value is used. | `number` | `3` | no |
| <a name="input_concurrent"></a> [concurrent](#input\_concurrent) | Limits how many jobs can run concurrently, across all registered runners. | `number` | `1` | no |
| <a name="input_config_path"></a> [config\_path](#input\_config\_path) | Config Path for Gitlab Runner | `string` | `"~/.gitlab-runner"` | no |
| <a name="input_context"></a> [context](#input\_context) | Single object for setting entire context at once.<br>See description of individual variables for details.<br>Leave string and numeric variables as `null` to use default value.<br>Individual variable settings (non-null) override settings in context object,<br>except for attributes, tags, and additional\_tag\_map, which are merged. | `any` | <pre>{<br>  "additional_tag_map": {},<br>  "attributes": [],<br>  "delimiter": null,<br>  "descriptor_formats": {},<br>  "enabled": true,<br>  "environment": null,<br>  "id_length_limit": null,<br>  "label_key_case": null,<br>  "label_order": [],<br>  "label_value_case": null,<br>  "labels_as_tags": [<br>    "unset"<br>  ],<br>  "name": null,<br>  "namespace": null,<br>  "regex_replace_chars": null,<br>  "stage": null,<br>  "tags": {},<br>  "tenant": null<br>}</pre> | no |
| <a name="input_delimiter"></a> [delimiter](#input\_delimiter) | Delimiter to be used between ID elements.<br>Defaults to `-` (hyphen). Set to `""` to use no delimiter at all. | `string` | `null` | no |
| <a name="input_descriptor_formats"></a> [descriptor\_formats](#input\_descriptor\_formats) | Describe additional descriptors to be output in the `descriptors` output map.<br>Map of maps. Keys are names of descriptors. Values are maps of the form<br>`{<br>   format = string<br>   labels = list(string)<br>}`<br>(Type is `any` so the map values can later be enhanced to provide additional options.)<br>`format` is a Terraform format string to be passed to the `format()` function.<br>`labels` is a list of labels, in order, to pass to `format()` function.<br>Label values will be normalized before being passed to `format()` so they will be<br>identical to how they appear in `id`.<br>Default is `{}` (`descriptors` output will be empty). | `any` | `{}` | no |
| <a name="input_enable_remote_cache"></a> [enable\_remote\_cache](#input\_enable\_remote\_cache) | Enable remote s3 cache config. | `bool` | `true` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Set to false to prevent the module from creating any resources | `bool` | `null` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | ID element. Usually used for region e.g. 'uw2', 'us-west-2', OR role 'prod', 'staging', 'dev', 'UAT' | `string` | `null` | no |
| <a name="input_executor"></a> [executor](#input\_executor) | Gitlab runner executor | `string` | `"shell"` | no |
| <a name="input_gitlab_token"></a> [gitlab\_token](#input\_gitlab\_token) | gitlab token to access gitlab api. | `string` | n/a | yes |
| <a name="input_id_length_limit"></a> [id\_length\_limit](#input\_id\_length\_limit) | Limit `id` to this many characters (minimum 6).<br>Set to `0` for unlimited length.<br>Set to `null` for keep the existing setting, which defaults to `0`.<br>Does not affect `id_full`. | `number` | `null` | no |
| <a name="input_label_key_case"></a> [label\_key\_case](#input\_label\_key\_case) | Controls the letter case of the `tags` keys (label names) for tags generated by this module.<br>Does not affect keys of tags passed in via the `tags` input.<br>Possible values: `lower`, `title`, `upper`.<br>Default value: `title`. | `string` | `null` | no |
| <a name="input_label_order"></a> [label\_order](#input\_label\_order) | The order in which the labels (ID elements) appear in the `id`.<br>Defaults to ["namespace", "environment", "stage", "name", "attributes"].<br>You can omit any of the 6 labels ("tenant" is the 6th), but at least one must be present. | `list(string)` | `null` | no |
| <a name="input_label_value_case"></a> [label\_value\_case](#input\_label\_value\_case) | Controls the letter case of ID elements (labels) as included in `id`,<br>set as tag values, and output by this module individually.<br>Does not affect values of tags passed in via the `tags` input.<br>Possible values: `lower`, `title`, `upper` and `none` (no transformation).<br>Set this to `title` and set `delimiter` to `""` to yield Pascal Case IDs.<br>Default value: `lower`. | `string` | `null` | no |
| <a name="input_labels_as_tags"></a> [labels\_as\_tags](#input\_labels\_as\_tags) | Set of labels (ID elements) to include as tags in the `tags` output.<br>Default is to include all labels.<br>Tags with empty values will not be included in the `tags` output.<br>Set to `[]` to suppress all generated tags.<br>**Notes:**<br>  The value of the `name` tag, if included, will be the `id`, not the `name`.<br>  Unlike other `null-label` inputs, the initial setting of `labels_as_tags` cannot be<br>  changed in later chained modules. Attempts to change it will be silently ignored. | `set(string)` | <pre>[<br>  "default"<br>]</pre> | no |
| <a name="input_log_format"></a> [log\_format](#input\_log\_format) | Specifies the log format | `string` | `"json"` | no |
| <a name="input_log_level"></a> [log\_level](#input\_log\_level) | Defines the log level | `string` | `"warn"` | no |
| <a name="input_max_uploaded_archive_size"></a> [max\_uploaded\_archive\_size](#input\_max\_uploaded\_archive\_size) | Limit, in bytes, of the cache archive being uploaded to cloud storage. | `string` | `"1000000000"` | no |
| <a name="input_name"></a> [name](#input\_name) | ID element. Usually the component or solution name, e.g. 'app' or 'jenkins'.<br>This is the only ID element not also included as a `tag`.<br>The "name" tag is set to the full `id` string. There is no tag with the value of the `name` input. | `string` | `null` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | ID element. Usually an abbreviation of your organization name, e.g. 'eg' or 'cp', to help ensure generated IDs are globally unique | `string` | `null` | no |
| <a name="input_regex_replace_chars"></a> [regex\_replace\_chars](#input\_regex\_replace\_chars) | Terraform regular expression (regex) string.<br>Characters matching the regex will be removed from the ID elements.<br>If not set, `"/[^a-zA-Z0-9-]/"` is used to remove all characters other than hyphens, letters and digits. | `string` | `null` | no |
| <a name="input_registration_token"></a> [registration\_token](#input\_registration\_token) | Registration token for registering the gitlab runner. | `string` | n/a | yes |
| <a name="input_runner_environment_variables"></a> [runner\_environment\_variables](#input\_runner\_environment\_variables) | Append or overwrite environment variables. | `list(string)` | `[]` | no |
| <a name="input_runner_tags"></a> [runner\_tags](#input\_runner\_tags) | Runner tags to allow for runner to execute only when called | `list(string)` | `[]` | no |
| <a name="input_shutdown_timeout"></a> [shutdown\_timeout](#input\_shutdown\_timeout) | Number of seconds until the forceful shutdown operation times out and exits the process. | `number` | `0` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | ID element. Usually used to indicate role, e.g. 'prod', 'staging', 'source', 'build', 'test', 'deploy', 'release' | `string` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Additional tags (e.g. `{'BusinessUnit': 'XYZ'}`).<br>Neither the tag keys nor the tag values will be modified by this module. | `map(string)` | `{}` | no |
| <a name="input_tenant"></a> [tenant](#input\_tenant) | ID element \_(Rarely used, not included by default)\_. A customer identifier, indicating who this instance of a resource is for | `string` | `null` | no |
| <a name="input_url"></a> [url](#input\_url) | Url of Gitlab host | `string` | `"https://gitlab.com"` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->