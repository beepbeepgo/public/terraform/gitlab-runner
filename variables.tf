###############################################################################
# Provider
###############################################################################

variable "url" {
  type        = string
  description = "Url of Gitlab host"
  default     = "https://gitlab.com"
}

variable "gitlab_token" {
  type        = string
  description = "gitlab token to access gitlab api."
  sensitive   = true
}

###############################################################################
# Register Gitlab Runner
###############################################################################

variable "runner_tags" {
  type        = list(string)
  description = "Runner tags to allow for runner to execute only when called"
  default     = []
}

variable "registration_token" {
  type        = string
  description = "Registration token for registering the gitlab runner."
  sensitive   = true
}

variable "access_level" {
  type        = string
  description = "Runner build directory"
  default     = "not_protected"
  validation {
    condition     = contains(["ref_protected", "not_protected"], var.access_level)
    error_message = "Valid values for var: access_level are (ref_protected, not_protected)."
  }
}

variable "allow_untagged" {
  type        = bool
  description = "Runner build directory"
  default     = false
}

###############################################################################
# Gitlab Runner Config Root
###############################################################################

variable "config_path" {
  type        = string
  description = "Config Path for Gitlab Runner"
  default     = "~/.gitlab-runner"
}

variable "concurrent" {
  type        = number
  description = "Limits how many jobs can run concurrently, across all registered runners."
  default     = 1
}

variable "check_interval" {
  type        = number
  description = "Defines the interval length, in seconds, between the runner checking for new jobs. The default value is 3. If set to 0 or lower, the default value is used."
  default     = 3
}

variable "shutdown_timeout" {
  type        = number
  description = "Number of seconds until the forceful shutdown operation times out and exits the process."
  default     = 0
}

variable "log_level" {
  type        = string
  description = "Defines the log level"
  default     = "warn"
  validation {
    condition     = contains(["debug", "info", "warn", "error", "fatal", "panic"], var.log_level)
    error_message = "Valid values for var: log_level are (debug, info, warn, error, fatal, panic)."
  }
}

variable "log_format" {
  type        = string
  description = "Specifies the log format"
  default     = "json"
  validation {
    condition     = contains(["runner", "text", "json"], var.log_format)
    error_message = "Valid values for var: log_format are (runner, text, json)."
  }
}

###############################################################################
# Gitlab Runner Config runners
###############################################################################

variable "executor" {
  type        = string
  description = "Gitlab runner executor"
  default     = "shell"
  validation {
    condition     = contains(["ssh", "shell", "parallels", "virtualbox", "docker", "kubernetes", "machine", "custom"], var.executor)
    error_message = "Valid values for var: executor are (ssh, shell, parallels, virtualbox, docker, kubernetes, machine, custom)."
  }
}

variable "build_dir" {
  type        = string
  description = "Absolute path to a directory where builds are stored in the context of the selected executor."
  default     = "/tmp"
}

variable "runner_environment_variables" {
  type        = list(string)
  description = "Append or overwrite environment variables."
  default     = []
}

###############################################################################
# Gitlab Runner Config runners cache 
###############################################################################

variable "enable_remote_cache" {
  type        = bool
  description = "Enable remote s3 cache config."
  default     = true
}

variable "cache_path" {
  type        = string
  description = "Name of the path to prepend to the cache URL"
  default     = ""
}

variable "cache_share" {
  type        = bool
  description = "Enables cache sharing between runners"
  default     = true
}

variable "max_uploaded_archive_size" {
  type        = string
  description = "Limit, in bytes, of the cache archive being uploaded to cloud storage."
  default     = "1000000000"
}

variable "aws_access_key" {
  type        = string
  description = "The access key specified for your S3 instance."
  default     = ""
}

variable "aws_secret_key" {
  type        = string
  description = "The secret key specified for your S3 instance."
  default     = ""
  sensitive   = true
}

variable "bucket_name" {
  type        = string
  description = "Name of the storage bucket where cache is stored."
  default     = ""
}

variable "bucket_region" {
  type        = string
  description = "Name of S3 region."
  default     = "us-east-2"
}
