module "label" {
  source     = "cloudposse/label/null"
  version    = "0.25.0"
  attributes = ["local"]

  context = module.this.context
}

resource "null_resource" "gitlab_runner_binary" {
  provisioner "local-exec" {
    command = "curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-${local.os}-${local.arch} && chmod +x /usr/local/bin/gitlab-runner" # Downloads and installs the GitLab runner binary, replacing `${local.os}` and `${local.arch}` with the values of the `os` and `arch` local variables.
  }
}

resource "gitlab_runner" "runner" {
  tag_list     = var.runner_tags    # Sets the tags for the GitLab runner.
  access_level = var.access_level   # Sets the access level for the GitLab runner.
  run_untagged = var.allow_untagged # Sets whether the GitLab runner should run untagged jobs.

  registration_token = var.registration_token # Sets the registration token for the GitLab runner.
}

resource "local_file" "config" {
  filename = "${pathexpand("~/.gitlab-runner")}/config.toml" # Sets the filename for the GitLab runner configuration file.
  content = templatefile("${path.module}/templates/config.tmpl", {
    # root
    concurrent = var.concurrent # Sets the maximum number of concurrent builds for the GitLab runner.
    log_level  = var.log_level  # Sets the log level for the GitLab runner.
    log_format = var.log_format # Sets the log format for the GitLab runner.

    check_interval   = var.check_interval   # Sets the check interval for the GitLab runner.
    shutdown_timeout = var.shutdown_timeout # Sets the shutdown timeout for the GitLab runner.
    # runners
    name      = var.name      # Sets the name for the GitLab runner.
    url       = var.url       # Sets the URL for the GitLab runner.
    executor  = var.executor  # Sets the executor for the GitLab runner.
    build_dir = var.build_dir # Sets the build directory for the GitLab runner.

    authentication_token = gitlab_runner.runner.authentication_token # Sets the authentication token for the GitLab runner.
    # runners cache
    enable_remote_cache       = var.enable_remote_cache       # Sets whether to enable the remote cache for the GitLab runner.
    max_uploaded_archive_size = var.max_uploaded_archive_size # Sets the maximum uploaded archive size for the GitLab runner.

    cache_path  = var.cache_path  # Sets the cache path for the GitLab runner.
    cache_share = var.cache_share # Sets the cache share for the GitLab runner.
    # runners cache s3
    aws_access_key = var.aws_access_key # Sets the AWS access key for the GitLab runner cache.
    aws_secret_key = var.aws_secret_key # Sets the AWS secret key for the GitLab runner cache.
    bucket_name    = var.bucket_name    # Sets the bucket name for the GitLab runner cache.
    bucket_region  = var.bucket_region  # Sets the bucket region for the GitLab runner cache.
  })
}

resource "null_resource" "install_gitlab_runner" {
  provisioner "local-exec" {
    command = "gitlab-runner install"
  }

  depends_on = [local_file.config, null_resource.gitlab_runner_binary]
  # Depends on local_file.config to ensure that the GitLab Runner configuration file is present before installation
}
