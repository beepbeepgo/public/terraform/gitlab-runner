# 1.0.0 (2023-02-25)


### Features

* gitlab-runner module ([cb5d0a3](https://gitlab.com/beepbeepgo/public/terraform/gitlab-runner/commit/cb5d0a3be42ba72327804879fe79705cb1c38d88))

# 1.0.0-init.1 (2023-02-25)


### Bug Fixes

* adds script for semantic-release ([0f197db](https://gitlab.com/beepbeepgo/public/terraform/gitlab-runner/commit/0f197db755748af2fc55f7dbf469b067b7ede515))
* allow ci terraform plan to fail ([3fc5b2c](https://gitlab.com/beepbeepgo/public/terraform/gitlab-runner/commit/3fc5b2c4f8e5b9c0044dcf839ac056bdc62b8868))
* remove provider from example ([e111f50](https://gitlab.com/beepbeepgo/public/terraform/gitlab-runner/commit/e111f5000b3097f1154014389831a895007fe303))
* remove variables from exmaple ([d222a98](https://gitlab.com/beepbeepgo/public/terraform/gitlab-runner/commit/d222a98af013fc967a126263135387a747f55d5d))


### Features

* gitlab-runner module ([52d9a80](https://gitlab.com/beepbeepgo/public/terraform/gitlab-runner/commit/52d9a8099316b6c695e0c0f3290034ba16ba7511))
