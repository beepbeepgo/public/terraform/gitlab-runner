locals {
  arch = data.external.detect.result.arch == "x86_64" ? "amd64" : data.external.detect.result.arch
  os   = data.external.detect.result.os == "Darwin" ? "darwin" : data.external.detect.result.os

  cache_path_error     = var.enable_remote_cache && var.cache_path == "" ? "cache_path must be specified when enable_remote_cache is true" : null
  bucket_name_error    = var.enable_remote_cache && var.bucket_name == "" ? "bucket_name must be specified when enable_remote_cache is true" : null
  aws_access_key_error = var.enable_remote_cache && var.aws_access_key == "" ? "aws_access_key must be specified when enable_remote_cache is true" : null
  aws_secret_key_error = var.enable_remote_cache && var.aws_secret_key == "" ? "aws_secret_key must be specified when enable_remote_cache is true" : null
}
