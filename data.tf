# This data block runs an external command to determine the operating system the Terraform is running on
data "external" "detect" {
  program = ["sh", "${path.module}/scripts/detect.sh"]
}